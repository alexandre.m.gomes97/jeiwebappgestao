﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JeiWebAppGestao.Data;
using JeiWebAppGestao.Models;
using JeiWebAppGestao.Filters;
using Microsoft.AspNetCore.Http;

namespace JeiWebAppGestao.Controllers
{
    [RoleFilter(Perfil = "Admin")]
    public class RifaTelesController : Controller
    {
        private readonly JeiWebAppGestaoContext _context;

        public RifaTelesController(JeiWebAppGestaoContext context)
        {
            _context = context;
        }

        // GET: RifaTeles
        public async Task<IActionResult> Index()
        {
            return View(await _context.RifaTele.Include(x => x.Admin).OrderBy(x => x.Id).ToListAsync());
        }

        // GET: RifaTeles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rifaTele = await _context.RifaTele
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rifaTele == null)
            {
                return NotFound();
            }

            return View(rifaTele);
        }

        // GET: RifaTeles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RifaTeles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int num,[Bind("Id,numAluno,Nome,Email,Data")] RifaTele rifaTele)
        {
            string Username = HttpContext.Session.GetString("Username");
            Admin admin = _context.Admin.FirstOrDefault(x => x.Username == Username);

            rifaTele.Admin = admin;
            rifaTele.Data = DateTime.Now;

            if (ModelState.IsValid)
            {
                for (int i = 0; i < num; i++)
                {
                    RifaTele nRifas = new RifaTele();
                    nRifas = rifaTele;
                    
                    _context.RifaTele.Add(nRifas);
                    _context.SaveChanges();
                }
                    
                 
                return RedirectToAction(nameof(Index));
            }
            return View(rifaTele);
        }

        // GET: RifaTeles/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var rifaTele = await _context.RifaTele.FindAsync(id);
        //    if (rifaTele == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(rifaTele);
        //}

        //// POST: RifaTeles/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,numAluno,Nome,Email,Data")] RifaTele rifaTele)
        //{
        //    if (id != rifaTele.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(rifaTele);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!RifaTeleExists(rifaTele.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(rifaTele);
        //}

        //// GET: RifaTeles/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var rifaTele = await _context.RifaTele
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (rifaTele == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(rifaTele);
        //}

        //// POST: RifaTeles/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var rifaTele = await _context.RifaTele.FindAsync(id);
        //    _context.RifaTele.Remove(rifaTele);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool RifaTeleExists(int id)
        {
            return _context.RifaTele.Any(e => e.Id == id);
        }
    }
}
