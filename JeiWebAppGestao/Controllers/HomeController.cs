﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JeiWebAppGestao.Models;
using System.Security.Cryptography;
using JeiWebAppGestao.Data;
using System.Text;
using Microsoft.AspNetCore.Http;
using JeiWebAppGestao.Filters;

namespace JeiWebAppGestao.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly JeiWebAppGestaoContext _context;

        public HomeController(ILogger<HomeController> logger, JeiWebAppGestaoContext context)
        {
            _logger = logger;
            _context = context;
        }
        [RoleFilter(Perfil = "Admin")]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string Username, string Password, string? ReturnUrl, [Bind("Username,Password,SAdmin")] Admin admin)
        {
            try
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(Password), 0, Encoding.UTF8.GetByteCount(Password));
                string passHash = Convert.ToBase64String(bytes);
                Admin uu = _context.Admin.SingleOrDefault(u => u.Username == Username);



                if (uu != null)
                {
                    if (uu.Password != passHash)
                    {
                        uu = null;
                        ModelState.AddModelError("Erro","As Passwords não são iguais!");
                    }


                    if (ModelState.IsValid)
                    {
                        HttpContext.Session.SetString("Username", uu.Username);
                        if (uu.SAdmin == true)
                        {
                            HttpContext.Session.SetString("Perfil", "SuperAdmin");
                        }
                        else
                        {
                            HttpContext.Session.SetString("Perfil", "Admin");
                        }
                   


                    return RedirectToAction("Index", "Home");
                    }
                }

                ViewData["Erro"] = "Username ou Password Errados";
                return View();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Email", "Um erro inesperado aconteceu, volte a tentar.");
                return View();
            }
        }


        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Perfil");
            HttpContext.Session.Remove("Username");
            return RedirectToAction("Login", "Home");
        }

        public static bool estaAutenticado(HttpContext contexto)
        {
            if (contexto.Session.GetString("Username") != null)
            {
                return true;
            }
            return false;
        }


    }
}
