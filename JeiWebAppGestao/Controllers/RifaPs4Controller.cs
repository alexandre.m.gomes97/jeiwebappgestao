﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JeiWebAppGestao.Data;
using JeiWebAppGestao.Models;
using JeiWebAppGestao.Filters;
using Microsoft.AspNetCore.Http;

namespace JeiWebAppGestao.Controllers
{
    [RoleFilter(Perfil = "Admin")]
    public class RifaPs4Controller : Controller
    {
        private readonly JeiWebAppGestaoContext _context;

        public RifaPs4Controller(JeiWebAppGestaoContext context)
        {
            _context = context;
        }

        // GET: RifaPs4
        public async Task<IActionResult> Index()
        {
            return View(await _context.RifaPs4.Include(x => x.Admin).OrderBy(x => x.Id).ToListAsync());
        }

        // GET: RifaPs4/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rifaPs4 = await _context.RifaPs4
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rifaPs4 == null)
            {
                return NotFound();
            }

            return View(rifaPs4);
        }

        // GET: RifaPs4/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RifaPs4/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int num,[Bind("Id,numAluno,Nome,Email,Data")] RifaPs4 rifaPs4)
        {
             string Username =  HttpContext.Session.GetString("Username");
            Admin admin = _context.Admin.FirstOrDefault(x => x.Username == Username);

            rifaPs4.Data = DateTime.Now;
            rifaPs4.Admin = admin;

            if (ModelState.IsValid)
            {
                for(int i = 0; i <= num; i++)
                _context.Add(rifaPs4);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Home");
            }
            return View(rifaPs4);
        }

        // GET: RifaPs4/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var rifaPs4 = await _context.RifaPs4.FindAsync(id);
        //    if (rifaPs4 == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(rifaPs4);
        //}

        //// POST: RifaPs4/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,numAluno,Nome,Email,Data")] RifaPs4 rifaPs4)
        //{
        //    if (id != rifaPs4.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(rifaPs4);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!RifaPs4Exists(rifaPs4.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(rifaPs4);
        //}

        //// GET: RifaPs4/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var rifaPs4 = await _context.RifaPs4
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (rifaPs4 == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(rifaPs4);
        //}

        //// POST: RifaPs4/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var rifaPs4 = await _context.RifaPs4.FindAsync(id);
        //    _context.RifaPs4.Remove(rifaPs4);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool RifaPs4Exists(int id)
        {
            return _context.RifaPs4.Any(e => e.Id == id);
        }
    }
}
