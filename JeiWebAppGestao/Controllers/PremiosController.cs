﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JeiWebAppGestao.Data;
using JeiWebAppGestao.Models;
using Microsoft.AspNetCore.Http;

namespace JeiWebAppGestao.Controllers
{
    public class PremiosController : Controller
    {
        private readonly JeiWebAppGestaoContext _context;

        public PremiosController(JeiWebAppGestaoContext context)
        {
            _context = context;
        }

        // GET: Premios
        public async Task<IActionResult> Index()
        {
            return View(await _context.Premios.Include(x => x.Admin).OrderBy(x => x.Id).ToListAsync());
        }

        // GET: Premios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var premios = await _context.Premios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (premios == null)
            {
                return NotFound();
            }

            return View(premios);
        }

        // GET: Premios/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Premios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nome,NumAluno,Date")] Premios premios)
        {
            string Username = HttpContext.Session.GetString("Username");
            Admin admin = _context.Admin.FirstOrDefault(x => x.Username == Username);

            premios.Date = DateTime.Now;
            premios.Admin = admin;

            if (ModelState.IsValid)
            {
                _context.Add(premios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(premios);
        }

        // GET: Premios/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var premios = await _context.Premios.FindAsync(id);
        //    if (premios == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(premios);
        //}

        //// POST: Premios/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,Nome,NumAluno,Date")] Premios premios)
        //{
        //    if (id != premios.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(premios);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!PremiosExists(premios.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(premios);
        //}

        //// GET: Premios/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var premios = await _context.Premios
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (premios == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(premios);
        //}

        //// POST: Premios/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var premios = await _context.Premios.FindAsync(id);
        //    _context.Premios.Remove(premios);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool PremiosExists(int id)
        {
            return _context.Premios.Any(e => e.Id == id);
        }
    }
}
