﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using JeiWebAppGestao.Models;

namespace JeiWebAppGestao.Data
{
    public class JeiWebAppGestaoContext : DbContext
    {
        public JeiWebAppGestaoContext (DbContextOptions<JeiWebAppGestaoContext> options)
            : base(options)
        {
        }

        public DbSet<JeiWebAppGestao.Models.SuperAdmin> SuperAdmin { get; set; }

        public DbSet<JeiWebAppGestao.Models.Admin> Admin { get; set; }

        public DbSet<JeiWebAppGestao.Models.Premios> Premios { get; set; }

        public DbSet<JeiWebAppGestao.Models.RifaPs4> RifaPs4 { get; set; }

        public DbSet<JeiWebAppGestao.Models.RifaTele> RifaTele { get; set; }
    }
}
