﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JeiWebAppGestao.Models
{
    public class Premios
    {
        public int Id { get; set; }

        [Display(Name= "Nome do Prémio")]   
        public string Nome { get; set; }

        [Display(Name = "Número de Aluno")]
        public int NumAluno{ get; set; }

        [Display(Name = "Data/Hora")]
        public DateTime Date { get; set; }


        public Admin Admin { get; set; }

    }
}
