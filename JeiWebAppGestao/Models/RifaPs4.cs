﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JeiWebAppGestao.Models
{
    public class RifaPs4
    {
        public int Id { get; set; }

        [Required]
        [Range(0, 99999)]
        [Display(Name = "Número de Aluno")]
        public int numAluno { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public Admin Admin { get; set; }
        public DateTime Data { get; set; }
    }
}
