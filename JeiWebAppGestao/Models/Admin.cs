﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JeiWebAppGestao.Models
{
    public class Admin
    {
        public int Id { get; set; }
        [Display(Name="Username do Admin")]
        public string Username { get; set; }

        public string Password { get; set; }

        public bool SAdmin { get; set; }

        public ICollection<RifaPs4> RifasPs4 { get; set; }

        public ICollection<RifaTele> RifasTele { get; set; }
    }
}
