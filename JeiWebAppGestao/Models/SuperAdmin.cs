﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JeiWebAppGestao.Models
{
    public class SuperAdmin
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public ICollection<Admin> Admins { get; set; }
    }
}
