﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JeiWebAppGestao.Migrations
{
    public partial class updateAdmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Admin_SuperAdmin_SAdminId",
                table: "Admin");

            migrationBuilder.DropIndex(
                name: "IX_Admin_SAdminId",
                table: "Admin");

            migrationBuilder.DropColumn(
                name: "SAdminId",
                table: "Admin");

            migrationBuilder.AddColumn<bool>(
                name: "SAdmin",
                table: "Admin",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SuperAdminId",
                table: "Admin",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Admin_SuperAdminId",
                table: "Admin",
                column: "SuperAdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_Admin_SuperAdmin_SuperAdminId",
                table: "Admin",
                column: "SuperAdminId",
                principalTable: "SuperAdmin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Admin_SuperAdmin_SuperAdminId",
                table: "Admin");

            migrationBuilder.DropIndex(
                name: "IX_Admin_SuperAdminId",
                table: "Admin");

            migrationBuilder.DropColumn(
                name: "SAdmin",
                table: "Admin");

            migrationBuilder.DropColumn(
                name: "SuperAdminId",
                table: "Admin");

            migrationBuilder.AddColumn<int>(
                name: "SAdminId",
                table: "Admin",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Admin_SAdminId",
                table: "Admin",
                column: "SAdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_Admin_SuperAdmin_SAdminId",
                table: "Admin",
                column: "SAdminId",
                principalTable: "SuperAdmin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
