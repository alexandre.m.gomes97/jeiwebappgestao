﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JeiWebAppGestao.Migrations
{
    public partial class updatePremio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumAluno",
                table: "Premios",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumAluno",
                table: "Premios");
        }
    }
}
