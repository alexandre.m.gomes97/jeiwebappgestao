﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JeiWebAppGestao.Migrations
{
    public partial class updatePremios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AdminId",
                table: "Premios",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Premios_AdminId",
                table: "Premios",
                column: "AdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_Premios_Admin_AdminId",
                table: "Premios",
                column: "AdminId",
                principalTable: "Admin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Premios_Admin_AdminId",
                table: "Premios");

            migrationBuilder.DropIndex(
                name: "IX_Premios_AdminId",
                table: "Premios");

            migrationBuilder.DropColumn(
                name: "AdminId",
                table: "Premios");
        }
    }
}
